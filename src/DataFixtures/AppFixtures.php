<?php

namespace App\DataFixtures;

use App\Entity\Media;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Rate;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private $manager;
    private $hasheur;
    private $faker;
    private $products = [];

    public function __construct(UserPasswordHasherInterface $hasheur)
    {
        $this->faker = Factory::create('fr_FR');
        $this->faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($this->faker));
        $this->hasheur = $hasheur;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this -> loadUser();
        $this -> loadProduct();
        $this -> loadOrder();

        $manager->flush();
    }

    
    private function loadUser() {
        $flo = new User();
        $flo ->setUsername("Redsilvernight")
        ->setPassword($this->hasheur->hashPassword($flo,"roule1grosboze"))
        ->setEmail("redsilvernight@gmail.com");
        
        $merwan = new User();
        $merwan -> setUsername("Moushouk")
        ->setPassword($this->hasheur->hashPassword($merwan,"L4BV3NTU2E"));
        
        $copinou = new User();
        $copinou->setUsername("test")
        ->setPassword($this->hasheur->hashPassword($copinou, "test"));
        
        $this->manager->persist($flo);
        $this->manager->persist($merwan);
        $this->manager->persist($copinou);
        $this->users = [$flo,$merwan,$copinou];
        
    }
    
    private function loadOrder() {
        foreach($this->users as $user) {
            for($o = 0; $o < 5; $o++) {
                $order = new Order();
                $order->setCreatedAt(new \DateTime())
                    ->setIsTransfert(0)
                    ->setOrderNumber("3X00831720641")
                    ->setPlugName($this->faker->text(20))
                    ->setPrice(mt_rand(100,300))
                    ->setStatus("ordered");
            
                foreach($this->products as $product) {
                    if(mt_rand(0,5) != 3)
                    {
                        $order->addProduct($product);
                    }
                }
                    
                $this->manager->persist($order);
                $user->addOrder($order);
            }
        }
    }

    private function loadProduct() {
        foreach ($this->users as $user)
        {
            for ($p=0; $p < mt_rand(5,10); $p++) {
                $media = new Media();
                $media->setPath("https://picsum.photos/200/300");
                $this->manager->persist($media);

                $product = new Product();
                $product -> setAmount(10)
                        ->setCreatedAt(new \DateTime())
                        ->setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui dicta minus molestiae vel beatae natus eveniet ratione temporibus aperiam harum alias officiis assumenda officia quibusdam deleniti eos cupiditate dolore doloribus!")
                        ->setIsInInventory(1)
                        ->setIsInWiki(mt_rand(0,1))
                        ->setName($this->faker->text(mt_rand(15,50)))
                        ->setType(mt_rand(0,1))
                        ->setWiki(1)
                        ->setOwner($user)
                        ->setMedias($media);

                $this->manager->persist($product);

                array_push($this->products, $product);

                $rate = $this->loadRate();
                
                $product->addRate($rate);
                $user->addRate($rate);
                $user->addProduct($product);
            }
        }
    }

    private function loadRate(): Rate {
        $new_rate = new Rate();
        $new_rate->setRate(mt_rand(1,10));

        $this->manager->persist($new_rate);

        return $new_rate;
    }
}
