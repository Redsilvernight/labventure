<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Assert\NotBlank(message="Le nom du plug ne peut pas être vide")
     * @Assert\Length(min=2,max=50,minMessage="Le nom du plug doit contenir au moins 2 caractere", maxMessage="Le nom du plug peut contenir jusqu'à 50 caractères")
     */
    private $plugName;

    #[ORM\Column(type: 'datetime')]
    /**
     * @Assert\DateTime(message="La date doit être au format datetime")
     */
    private $createdAt;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Assert\NotBlank(message="Le numéro de suivi ne peut pas être vide")
     * @Assert\Length(min=1,max=20,minMessage="Le suivi doit contenir au moins 1 caractere", maxMessage="Le suivi peut contenir jusqu'à 20 caractères")
     */
    private $orderNumber;

    #[ORM\Column(type: 'integer')]
    /**
     * @Assert\NotBlank(message="Le prix ne peut pas être vide")
     * @Assert\Positive(message="Le prix doit être supérieur à 0")
     */
    private $price;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy:'orders',  cascade: ["persist"])]
    /**
     * @Assert\Type(type="App\Entity\User", message="Désolé ça n'a pas marché")
     */
    private $owner;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy:'orders',  cascade: ["persist"])]
    private $products;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Assert\Choice(choices={"orderer","processing","delivery"}, message="L'opération a mal tournée.")
     */
    private $status;

    #[ORM\Column(type: 'boolean', nullable: true)]
    /**
     * @Assert\Type(type="boolean", message="Aie, ça à mal fonctionné...")
     */
    private $isTransfert;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlugName(): ?string
    {
        return $this->plugName;
    }

    public function setPlugName(string $plugName): self
    {
        $this->plugName = $plugName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIsTransfert(): ?bool
    {
        return $this->isTransfert;
    }

    public function setIsTransfert(?bool $isTransfert): self
    {
        $this->isTransfert = $isTransfert;

        return $this;
    }
}
