<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length:255, nullable: true)]
    /**
     * @Assert\NotBlank(groups={"product:new"},message="Le nom du produit ne peut pas être vide")
     * @Assert\Length(min=2,max=50,minMessage="Le nom du produit doit contenir au moins 2 caractere", maxMessage="Le nom du produit peut contenir jusqu'à 50 caractères")
     */
    private $name;

    #[ORM\Column(type: 'text', nullable: true)]
    /**
     * @Assert\NotBlank(groups={"product:edit"}, message="La description du produit ne peut pas être vide")
     * @Assert\Length(min=2,max=10000,minMessage="La description du produit doit contenir au moins 2 caractere", maxMessage="La description du produit peut contenir jusqu'à 10000 caractères")
     */
    private $description;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $createdAt;

    #[ORM\Column(type: 'string', length:255, nullable: true)]
    private $type;

    #[ORM\Column(type:'boolean', nullable: true)]
    /**
     * @Assert\Type(type="boolean", message="Aie, ça à mal fonctionné...")
     */
    private $isInWiki;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy:'products')]
    private $owner;

    #[ORM\Column(type: 'integer', nullable: true)]
    /**
     * @Assert\NotBlank(groups={"product:new"},message="La quantité ne peut pas être vide")
     * @Assert\Positive(message="La quantité doit être supérieure à 0")
     */
    private $amount;

    #[ORM\Column(type:'boolean', nullable: true)]
    /**
     * @Assert\Type(type="boolean", message="Aie, ça à mal fonctionné...")
     */
    private $isInInventory;

    #[ORM\OneToOne(targetEntity: Media::class, cascade: ['persist', 'remove'])]
    private $medias;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Rate::class,  cascade:["remove"], orphanRemoval : true)]
    private $rates;

    #[ORM\Column(type:'boolean', nullable: true)]
    /**
     * @Assert\Type(type="boolean", message="Aie, ça à mal fonctionné...")
     */
    private $wiki;

    #[ORM\ManyToMany(targetEntity: Order::class, mappedBy: 'products', cascade: ["persist"])]
    private $orders;

    public function __construct()
    {
        $this->rates = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getTypeStr(): ?string
    {
        if($this->getType() == "1")
        {
            return "Indica";
        }
        else {
            return "Sativa";
        }
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsInWiki(): ?bool
    {
        return $this->isInWiki;
    }

    public function setIsInWiki(bool $isInWiki): self
    {
        $this->isInWiki = $isInWiki;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getIsInInventory(): ?bool
    {
        return $this->isInInventory;
    }

    public function setIsInInventory(bool $isInInventory): self
    {
        $this->isInInventory = $isInInventory;

        return $this;
    }

    public function getMedias(): ?Media
    {
        return $this->medias;
    }

    public function setMedias(?Media $medias): self
    {
        $this->medias = $medias;

        return $this;
    }

    public function getRates(): int
    {
        if(count($this->rates) > 0) {
            $count = 0;
            foreach($this->rates as $rates)
            {
                $count += $rates->getRate();
                $rate = $count/count($this->rates);
            }
            return $rate;
        }
        else {
            return 0;
        }
    }

    /**
     * @return Collection<int, Product>
     */
    public function getAllRates(): Collection
    {
        return $this->rates;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates[] = $rate;
            $rate->setProduct($this);
        }

        return $this;
    }

    public function removeRate(Rate $rate): self
    {
        if ($this->rates->removeElement($rate)) {
            // set the owning side to null (unless already changed)
            if ($rate->getProduct() === $this) {
                $rate->setProduct(null);
            }
        }

        return $this;
    }

    public function getWiki(): ?bool
    {
        return $this->wiki;
    }

    public function setWiki(bool $wiki): self
    {
        $this->wiki = $wiki;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->addProduct($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            $order->removeProduct($this);
        }

        return $this;
    }

}
