<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderType;
use App\Form\ProductType;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use App\Service\OrderManager;
use App\Service\ProductManager;
use App\Service\TrackingManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    private $productRepository;
    private $manager;
    private $orderRepository;

    public function __construct(EntityManagerInterface $manager, ProductRepository $productRepository, OrderRepository $orderRepository)
    {
        $this->productRepository = $productRepository;
        $this->manager = $manager;
        $this->orderRepository = $orderRepository;
    }
    /**
     * @Route("/inventory", name="app_user_inventory")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function index(Request $request, ProductManager $productManager, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $request->getSession()->get("username")]);

        $product = new Product();
        $form = $this->createForm(ProductType::class, $product, ["validation_groups" => ["product:new", "Default"]]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productManager->addInventory($user, $form, $product);
        }

        return $this->render('user/inventory.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tracking/{id}", name="user_tracking")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function getTracking(Order $order, TrackingManager $trackingManager, OrderManager $orderManager)
    {
        $response = $trackingManager->getTracking($order->getOrderNumber());

        if (200 !== $response->getStatusCode()) {
            return $this->render('user/_tracking.html.twig', [
                'error' => "500",
                "events" => []
            ]);
        } else {
            $orderManager->updateOrderTrackeur($response, $order);
            $response = $response->toArray()['shipment']['event'];

            return $this->render('user/_tracking.html.twig',[
                'events' => $response,
                'order' => $order
            ]);
        }
    }

    /**
     * @Route("/product", name="inventory_product")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function inventoryProduct(UserRepository $userRepository, Request $request)
    {
        $user = $userRepository->findOneBy(['username' => $request->getSession()->get('username')]);
        return $this->render('user/_inventory-products.html.twig', [
            "products" => $this->productRepository->findByInventory($user)
        ]);
    }

    /**
     * @Route("/order", name="inventory_order")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function inventoryOrder(UserRepository $userRepository, Request $request)
    {
        $user = $userRepository->findOneBy(['username' => $request->getSession()->get('username')]);
        return $this->render('user/_inventory-orders.html.twig', [
            "orders" => $this->orderRepository->findBy(['owner' => $user])
        ]);
    }

}
