<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Service\ProductManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppController extends AbstractController
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    /**
     * @Route("/accueil", name="app_home")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function index(): Response
    {
        $products = $this->productRepository->findBy(["wiki" => true],['name'=>'ASC']);
        return $this->render('app/home.html.twig',[
            "products" => $products	
        ]);
    }

    /**
     * @Route("/accueil/search/{name}", name="app_search")
     */
    public function search($name): Response
    {
        $products = $this->productRepository->findSearch($name);
        return $this->render('product/_search.html.twig', [
            "products" => $products
        ]);
    }

    /**
     * @Route("/print/{id}", name="app_print")
     */
    public function print(Product $product, ProductManager $productManager): Response
    {
        $productManager->printTag($product->getName(), $product->getAmount());

        return $this->redirectToRoute('app_user_inventory');
    }
}
