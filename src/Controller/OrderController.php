<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use App\Repository\UserRepository;
use App\Service\OrderManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $manager;
    private $userRepository;

    public function __construct(EntityManagerInterface $manager, UserRepository $userRepository)
    {
        $this->manager = $manager;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/order", name="app_inventory-order")
     */
    public function inventoryOrder(Request $request, OrderManager $orderManager): Response
    {
        $user = $this->userRepository->findOneBy(["username" => $request->getSession()->get("username")]);

        $order = new Order();
        $formOrder = $this->createForm(OrderType::class, $order);
        $formOrder->handleRequest($request);

        if ($formOrder->isSubmitted() && $formOrder->isValid()) {
            $orderManager->addOrder($user, $order);
            return $this->redirectToRoute('app_inventory-order', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/order.html.twig', [
            'formOrder' => $formOrder
        ]);
    }

    /**
     * @Route("/order/products/{id}", name="order_products")
     */
    public function orderProduct(Order $order): Response
    {
        return $this->render('order/_listProduct.html.twig', [
            "products" => $order->getProducts()
        ]);
    }

    /**
     * @Route("/order/transfert/{id}", name="order_transfert")
     */
    public function orderTransfert(Order $order, UserRepository $userRepository, Request $request): Response
    {
        $user = $userRepository->findOneBy(['username' => $request->getSession()->get('username')]);
        $order->setIsTransfert(True);
        foreach($order->getProducts() as $product) {
            $product->setIsInInventory(True)
                    ->setCreatedAt(new \DateTime())
                    ->setWiki(False);
            
            $user->addProduct($product);

        }
        $this->manager->flush();
    
        return $this->json(["code" => "200"]);
    }

    /**
     * @Route("/order/remove/{id}", name="app_order_delete")
     */
    public function delete(Order $order, UserRepository $userRepository, Request $request): Response
    {
        $user = $userRepository->findOneBy(['username' => $request->getSession()->get('username')]);
        $user->removeOrder($order);
        $this->manager->remove($order);
        $this->manager->flush();

        return $this->redirectToRoute('app_inventory-order');
    }
}
