<?php

namespace App\Controller;

use App\Entity\Media;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Rate;
use App\Form\OrderType;
use App\Form\ProductType;

use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use App\Repository\RateRepository;
use App\Service\OrderManager;
use App\Service\ProductManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    private $productRepository;
    private $productManager;
    private $manager;
    private $rateRepository;

    public function __construct(ProductRepository $productRepository, ProductManager $productManager, EntityManagerInterface $manager, RateRepository $rateRepository)
    {
        $this->productRepository = $productRepository;
        $this->productManager = $productManager;
        $this->manager = $manager;
        $this->rateRepository = $rateRepository;
    }

    /**
     * @Route("/{id}", name="app_product_show")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
     */
    public function show(Product $product, Request $request, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $request->getSession()->get("username")]);

        $inventoryProduct = $this->productRepository->findOneBy(["name" => $product->getName(), "isInInventory" => True, "owner" => $user]);
        
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'inventory' => $inventoryProduct,
        ]);
    }

    /**
     * @Route("/wiki/{name}", name="app_product_getWiki")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
     */
    public function getWiki($name)
    {
        $wikiProduct = $this->productRepository->findOneBy(["name" => $name, "wiki" => true]);

        return $this->redirectToRoute("app_product_show",["id" => $wikiProduct->getId()]);
    }

    /**
     * @Route("/{id}/rate", name="app_product_rate")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
     */
    public function rate(Product $product,Request $request)
    {
        $userRate = $this->rateRepository->findOneBy(["user" => $this->getUser(), "product" => $product]);
        
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formRate = $form->get("rate")->getData();
            if ($formRate != null) {
                if ($userRate != null) {
                    $product->removeRate($userRate);
                }
                $rate = new Rate();
                $rate->setProduct($product)
                    ->setUser($this->getUser())
                    ->setRate($formRate);

                $this->manager->persist($rate);
                $product->addRate($rate);

                $this->manager->flush();
                $userRate = $rate;
            }
        }
        return $this->render('product/rate.html.twig', [
            "rates" => $this->rateRepository->findBy(["product" => $product->getId()]),
            "form" => $form->createView(),
            "userRate" => $userRate,
            "product" => $product
        ]);
    }

    /**
     * @Route("/{id}/addWiki", name="app_product_wiki")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
     */
    public function addWiki(Request $request, Product $product, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $request->getSession()->get("username")]);
        $error = null;

        $form = $this->createForm(ProductType::class, $product, ["validation_groups" => ["product:edit", "Default"]]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formFile = $form->get("medias")->getData();
            $formRate = $form->get("rate")->getData();
            if($form->isValid())
            {
                $this->productManager->addWiki($user, $form, $product, $formFile);
                return $this->redirectToRoute('app_user_inventory', [], Response::HTTP_SEE_OTHER);

            }
            if($formFile === null) {
                $error["media"] = "Montrer-nous votre produit !";
            }
            if($formRate === null) {
                $error["rate"] = "Notez votre produit !";
            }
        }
        return $this->renderForm('product/addWiki.html.twig', [
            'product' => $product,
            'form' => $form,
            "error" => $error
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_product_edit")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
     */
    public function edit(Request $request, Product $product, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(["username" => $request->getSession()->get("username")]);

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->productManager->edit($form, $product);
            return $this->redirectToRoute('app_product_show',['id' => $product->getId()]);
        }

        return $this->renderForm('product/edit.html.twig', [
            'product' => $product,
            'form' => $form
        ]);
    }

    /**
     * @Route("/deleteStock/{id}", name="app_product_deleteStock")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
    */
    public function deleteStock(Product $product): Response
    {
        $this->productRepository->removeStock($product);

        return $this->redirectToRoute('app_user_inventory');
    }

    /**
     * @Route("/delete/{id}", name="app_product_delete")
     * @IsGranted("ROLE_USER", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !")
    */
    public function delete(Product $product): Response
    {
        $products = $this->productRepository->findBy(['name' => $product->getName()]);
        foreach ($products as $product) {
            $product->setIsInWiki(false)
                    ->setWiki(false);
        }

        $this->manager->flush();

        return $this->redirectToRoute('app_home');
    }
}
