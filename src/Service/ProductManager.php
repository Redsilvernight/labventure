<?php
namespace App\Service;

use App\Entity\Media;
use App\Entity\Product;
use App\Entity\Rate;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProductManager
{
    private $productRepository;
    private $manager;
    private $product;
    private $user;
    private $parameter;
    private $mediaManager;

    public function __construct(ProductRepository $productRepository, EntityManagerInterface $manager, ParameterBagInterface $parameter, MediaManager $mediaManager)
    {
        $this->productRepository = $productRepository;
        $this->manager = $manager;
        $this->parameter = $parameter;
        $this->mediaManager = $mediaManager;
    }

    public function addInventory($user,$form,$product): void
    {
        $product = $this->isInWiki($product);

        if ($form->get("isPrintTag")->getData() === true) {
            $this->printTag($product->getName(), $product->getAmount());
        }

        $product->setCreatedAt(new \Datetime())
            ->setIsInInventory(true);
        
        $user->addProduct($product);

        $this->manager->persist($product);
        $this->manager->flush();

        return ;
    }

    public function isInWiki(Product $product): Product
    {
        $verify = $this->productRepository->findOneBy(["name" => $product->getName(), "wiki" => true]);
        if ($verify === null) {
            $product->setIsInWiki(false);
        } else {
            $product->setIsInWiki(true);
        }

        return $product;
    }

    public function addWiki($user,$form,$product,$formFile): void
    {
        $this->user = $user;
        $this->product = $product;
        $this->setRate($form);

        $this->product->setIsInWiki(true)
                    ->setType($form->get("type")->getData())
                    ->setWiki(true);

        $this->mediaManager->new($formFile, $this->product);

        $this->updateStatus();

        $this->manager->flush();

        return ;
    }

    private function setRate($form): void
    {
        $formRate = $form->get("rate")->getData();
        $rate = new Rate();
        $rate->setUser($this->user)
            ->setRate($formRate);

        $this->product->addRate($rate);
        $this->manager->persist($rate);
    }

    private function updateStatus(): void
    {
        $addProduct = $this->productRepository->findThis($this->product->getName());
        foreach ($addProduct as $productAdd) {
            $productAdd->setIsInWiki(true);
        }
    }

    public function printTag($name, $amount): void
    {
        $client = new \WebSocket\Client("ws://92.150.201.16:8000");
        $client->text($amount . "-" . $name);
    }

    public function edit($form,$product): void
    {
        $formFile = $form->get("medias")->getData();
        $this->mediaManager->new($formFile,$product);

        $this->manager->flush();

        return ;
    }
}