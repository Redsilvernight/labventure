<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class TrackingManager
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    public function getTracking(string $orderNumber)
    {
        $url = 'https://api.laposte.fr/suivi/v2/idships/' . $orderNumber . '?lang=fr_FR';

        $response = $this->client->request(
            "GET",
            $url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    "X-Okapi-Key" => "lN/Yl6yPqtUXsY+ebeUmG00LXRbYju/+qajpqtJ5BNm8YKUu6ShMLvEguzsadkcy"
                ]
            ]
        );

        return $response;
    }
}