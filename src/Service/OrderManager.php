<?php

namespace App\Service;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OrderManager
{
    private $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    public function addOrder($user, $newOrder): void
    {
        $newOrder->setOwner($user)
                ->setCreatedAt(new \DateTime())
                ->setStatus("ordered");
        
        foreach($newOrder->getProducts() as $product) {
            if($product->getName() === null){
                $newOrder->removeProduct($product);
            }
        }
        
        $this->manager->persist($newOrder);
        $this->manager->flush();

        return;

    }

    public function updateOrderTrackeur(ResponseInterface $response, Order $order): void
    {
        if ($response->toArray()['shipment']['isFinal'] === true) {
            if ($order->getStatus() != "delivery") {
                $order->setStatus("delivery");
                $order->setIsTransfert(0);
            }
        } else {
            $order->setStatus("processing");
        }

        $this->manager->flush();

        return;
    }

}