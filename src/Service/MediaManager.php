<?php

namespace App\Service;

use App\Entity\Media;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MediaManager
{
    private $manager;
    private $parameter;

    public function __construct(EntityManagerInterface $manager, ParameterBagInterface $parameter)
    {
        $this->manager = $manager;
        $this->parameter = $parameter;
    }

    public function new($formMedia, Product $product)
    {
        if ($formMedia != null) {
            $path = md5(uniqid()) . '.' . $formMedia->guessExtension();
            $formMedia->move($this->parameter->get('images_directory'), $path);
            $media = new Media();
            $media->setPath($path);

            $this->manager->persist($media);
            $product->setMedias($media);
        }
    }
}