<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plugName', TextType::class, [
                "label" => "D'où ça vient ?"
            ])
            ->add('orderNumber', TextType::class, [
                "label" => "Numéro de suivie"
            ])
            ->add('price', MoneyType::class, [
                "label" => "Prix total"
            ])
            ->add('products', CollectionType::class, [
                'entry_type' => ProductOrderType::class,
                'entry_options' => [
                    'attr' => [
                        "class" => "d-flex"
                    ]
                ],
                'label' => "Produits",
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'delete_empty' => true,
            ])
            ->add('delete', TextType::class, [
                "mapped"=> false,
                "required" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
