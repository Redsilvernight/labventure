<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                "label" => "Variété"
            ])
            ->add('amount',NumberType::class, [
                "label" => "Quantité (g)"
            ])
            ->add('isPrintTag', CheckboxType::class, [
                "label" => "Imprimer une étiquette",
                "mapped" => false,
                "required" => false,
            ])
            ->add('delete', HiddenType::class, [
                "mapped" => false,
                "required" => false,
            ])
            ->add('medias', FileType::class, [
                "mapped" => false,
                "required" => false,
            ])
            ->add('type', ChoiceType::class, [
                "choices" => [
                    "Indica" => true,
                    "Sativa" => "Sativa"
                ],
                "expanded" => true,
                "multiple" => false,

            ])
            ->add('description', TextareaType::class, [
                "attr" => [
                    "rows" => "10"
                ]
            ])
            ->add('rate', TextType::class, [
                "mapped" => false,
                "required" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
