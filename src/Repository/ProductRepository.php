<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Product $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeStock(Product $entity, bool $flush = true): void
    {
        $entity->setIsInInventory(false);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findByInventory(User $user)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.owner = :owner', 'p.isInInventory = true')
            ->setParameter('owner', $user)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findThis(string $name)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name = :name', 'p.isInWiki = false')
            ->setParameter('name', $name)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
    
    public function findSearch(string $search)
    {
        return $search == "none" ? $this->findBy(["wiki" => true], ['name' => 'ASC']) : $this->createQueryBuilder('p')
            ->where('p.name LIKE :search')
            ->andWhere('p.wiki = true')
            ->setParameter('search', $search.'%')
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
