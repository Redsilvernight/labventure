$(document).ready(function() {
    $('.order-slide').click(function (e) {
        var id = this.id.split("-")[2];
        var url = '/user/tracking/'+id;
        if($(this).children().hasClass('order-slide-up')) {
            orderSlideDown(id);
            return;
        }
        $('#order-slide-icon-' + id).removeClass("fa-angle-down");
        $('#order-slide-icon-' + id).addClass("fa-angle-up").addClass("order-slide-up");
            
        axios.get(url).then(function(response) {
            if(response.request.responseURL.search(url) != -1) {
                $('#tracking-'+id).html(response.data);
                $('#tracking-'+id).slideDown();
            }else {
                window.location.href = "/logout";
            }  
        })
    })
    
    $('.cardbox').click(function (e) {
        var id = $(this).data("id").split("-")[1];
        var url = "/order/products/"+id;
        $('.order-products').removeClass("d-none");
        axios.get(url).then(function(response) {
            if(response.request.responseURL.search(url) != -1) {
                $('#order-list-products').html(response.data);
            }else {
                window.location.href = "/logout";
            }
        })
    })

    $('.close-order-products').click(function (e) {
        $('.order-products').addClass("d-none");
        $('#order-list-products').empty();
    })

    $('.close-transfert-products').click(function (e) {
        $('#transfert-products').addClass("d-none");
    })

    $('#valid-transfert').click(function (e) {
        var id = $('#valid-transfert').data("order");
        var url = "/order/transfert/"+id;
        axios.get(url);
        $('#transfert-products').addClass("d-none");
    })
})

function orderSlideDown(id)
{
    $('#tracking-'+id).slideUp();
    $('#tracking-'+id).empty();
    $('#order-slide-icon-' + id).removeClass("fa-angle-up").removeClass("order-slide-up");
    $('#order-slide-icon-' + id).addClass("fa-angle-down");
    return;
}