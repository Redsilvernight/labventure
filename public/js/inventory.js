$(document).ready(function() {
    var url = "/user/product";
    axios.get(url).then(function(response) {
        if(response.request.responseURL.search(url) != -1) {
            $('#div-inventory').html(response.data);
        }else {
                window.location.href = "/logout";
        }             
    });

    $('#add-product').click(function() {
        if ($("#add-product > i").hasClass('transformClose')) {
            $("#add-product > i").removeClass('transformClose');
            $("#add-product > i").addClass('transformOpen');
            
            $("#div-add-toggle").slideUp();
            $("#div-btn-add").removeClass("div-btn-switch-add--deploy");
        }
        else {
            $("#div-btn-add").addClass("div-btn-switch-add--deploy");
            $("#add-product > i").addClass("transformClose");
            $("#add-product > i").removeClass('transformOpen');

            $("#div-add-toggle").slideDown();

        }
    })
})