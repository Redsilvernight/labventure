$(document).ready(function() {
    $('#btn-openNav').on('tap',function(e) {
        $(this).hide();
        $('.nav-icon').show();
        $("#btn-closeNav").removeClass("d-none");
        $("#nav-deploy").css("visibility","visible");
        $('#nav-deploy').addClass("nav-deployed");
    })

    $('#btn-closeNav').on('tap',function(e) {
        $('.nav-icon').hide();
        $("#btn-openNav").show();
        $(this).addClass("d-none");
        setTimeout(function() {
            $("#nav-deploy").css("visibility","hidden");
        },900);
        $('#nav-deploy').removeClass("nav-deployed");
    })

    $('.nav-link').on('tap', function(e) {
        var url = $(this).data('redirect');
        window.location.replace(url);
    })
})