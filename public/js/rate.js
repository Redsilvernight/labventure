$(document).ready(function() {
    $('.stars').click(function(event) {
        var rate = $(this).attr("id").split("-")[1];
        $('.stars').each(function() {
            $(this).removeClass('other-font-color')
            $(this).removeClass('fas');
            $(this).addClass('far');
        })
        for(var i = 0; i <= rate; i++) {
            $('#star-'+i).addClass('fas');
            $('#star-'+i).removeClass('far');
            $('#star-'+i).toggleClass('other-font-color');
        }

        $('#product_rate').val(rate);
    })
})