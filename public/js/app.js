function previewImage(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var content = $(input).data("preview")
        reader.onload = function(e) {
            $('#addWiki-preview > i').remove();
            $('.product-preview > img').attr('src', e.target.result).removeClass("d-none");

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function() {
    $('#btn-printTag').on('click', function(e) {
        var id = $(this).data("id");
        var url = "/print/" + id;
        
        axios.get(url).then(function(response) {
            if(response.request.responseURL.search(url) != -1) {
                $('#btn-print-tag').removeClass('d-none');
            }else {
                window.location.href = "/logout";
            }
        });
    });

    $('#close-print-tag').click(function(e) {
        $('#btn-print-tag').addClass('d-none');
    });

    $('#search-product').on('input',function(e) {
        var search = $(this).val();
        if(search == "") {
            search = "none";
        }
        var url = "/accueil/search/" + search;
            axios.get(url).then(function(response) {
                if(response.request.responseURL.search(url) != -1) {
                    $('#show-products').empty();
                    $('#show-products').html(response.data);
                }else {
                    window.location.href = "/logout";
                }
            })
    });

    $('#search-btn').on('click', function(e) {
        $('#search-btn').toggle();
        $('#search-close').removeClass('d-none');
        $('#div-content-search').slideDown();
        $('#search-product').focus();
    })
    $('#search-close').on('click', function(e) {
        $('#search-close').addClass('d-none');
        $('#div-content-search').slideUp();
        $('#search-btn').toggle();
    })

    $('.link-disabled').click(function (e) {
        e.preventDefault();
        $('#link-disabled').removeClass('d-none');
        $('#link-disabled').slideDown();
        setTimeout(function() {
            $('#link-disabled').slideUp();
        },3000);

    })

})