$(document).ready(function() {
    $('#add-order').click(function() {
        if ($("#add-order > i").hasClass('transformClose')) {
            $("#add-order > i").removeClass('transformClose');
            $("#add-order > i").addClass('transformOpen');
            
            $("#div-addOrder-toggle").slideUp();
            $("#div-btnOrder-add").removeClass("div-btn-switch-add--deploy");
        }
        else {
            $("#div-btnOrder-add").addClass("div-btn-switch-add--deploy");
            $("#add-order > i").addClass("transformClose");
            $("#add-order > i").removeClass('transformOpen');

            $("#div-addOrder-toggle").slideDown();

        }
    });

    jQuery('.add-field').click(function (e) {
        var list = jQuery(jQuery(this).attr('data-list-selector'));
        var counter = list.data('widget-counter') || list.children().length;
        var newWidget = list.attr('data-prototype');
        $('#count-product').html("("+counter+")");
        if(counter == 1) {
            $('#list-products').removeClass('d-none');
        }
        if(counter - 1 >= 0) {
            validField(counter-1);
        }
        newWidget = newWidget.replace(/__name__/g, counter);
        counter++; 
        list.data('widget-counter', counter);
        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget);
        newElem.appendTo(list);
        $('#li_products_').attr('id','li_products_' + (counter-1));
    });

    $('#icon-count-product').click(function (e) {
        if($(this).hasClass('fa-angle-down')) {
            $('#icon-count-product').removeClass('fa-angle-down').addClass('fa-angle-up');
            $('#list-products').slideDown();
        }
        else {
            $('#icon-count-product').removeClass('fa-angle-up').addClass('fa-angle-down');
            $('#list-products').slideUp();
        }
    })
})

function removeField(btn) {
    $('#li_products_' + btn.parentNode.id.split("_")[2]).remove();
    $(btn.parentNode).remove();
    $('#count-product').html("("+btn.parentNode.id.split("_")[2]+")");
}

function validField(counter) {
    $('#li_products_' + counter).hide();
    var productName = $('#order_products_' + counter + '_name').val();
    var productAmount = $('#order_products_' + counter + '_amount').val();
    var li = '<li id="li_list_'+counter+'">'+productAmount+'g '+productName+'<span onclick="removeField(this)" class="ms-5 danger-color btn-cancel">Annuler</span></li>';
    $('#list-products').append(li);
}
